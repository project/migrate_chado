<?php

/**
 * @file
 * Support for dbxref destinations.
 */

/**
 * Destination class implementing migration into dbxref table.
 */
class MigrateDestinationChadoDbxref extends MigrateDestinationChado {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct('dbxref');
  }

  /**
   * Get key schema for the dbxref destination.
   *
   * @see MigrateDestination::getKeySchema
   *
   * @return array
   *   Returns the key schema.
   */
  public static function getKeySchema() {
    return array(
      'dbxref_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'dbxref ID',
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fields($migration = NULL) {
    return [
      'db_id' => t('Links to a db record'),
      'accession' => t('Local part of the identifier, guaranteed by the db authority to be unique for that db'),
      'version' => t('Version'),
      'description' => t('Description (optional)'),
    ];
  }

}
