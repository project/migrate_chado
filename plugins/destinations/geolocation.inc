<?php

/**
 * @file
 * Support for geolocation destinations.
 */

/**
 * Destination class implementing migration into nd_geolocation table.
 */
class MigrateDestinationChadoGeolocation extends MigrateDestinationChado {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct('nd_geolocation');
  }

  /**
   * Get key schema for the geolocation destination.
   *
   * @see MigrateDestination::getKeySchema
   *
   * @return array
   *   Returns the key schema.
   */
  public static function getKeySchema() {
    return array(
      'nd_geolocation_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'geolocation ID',
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fields($migration = NULL) {
    return [
      'latitude' => t('The decimal latitude coordinate of the georeference, using positive and negative sign to indicate N and S, respectively'),
      'longitude' => t('The decimal longitude coordinate of the georeference, using positive and negative sign to indicate E and W, respectively'),
      'altitude' => t('The altitude (elevation) of the location in meters (optional)'),
      'geodetic_datum' => t('Geodetic datum (most likely "WGS84") (optional)'),
      'description' => t('A textual representation of the location, if this is the original georeference, optional if the original georeference is available in lat/long coordinates'),
    ];
  }

}
