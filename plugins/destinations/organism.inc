<?php

/**
 * @file
 * Support for organism destinations.
 */

/**
 * Destination class implementing migration of taxons (organism table).
 */
class MigrateDestinationChadoOrganism extends MigrateDestinationChado {

  /**
   * In mapping mode, we only look for correspondences in local database.
   *
   * @var bool
   */
  protected $mapping;

  /**
   * In creation mode, we allow creation of new organisms.
   *
   * If both $mapping and $creation are true (hybrid mode), we first look for an
   * existing correspondence in the local database, and create new organisms if
   * none was found.
   *
   * Caution: in hybrid mode, both previously existing organisms and those
   * effectively imported will be deleted! If this is not the expected
   * behaviour, set the mode to creation only, and use the technique described
   * in the issue below.
   *
   * @var bool
   *
   * @see https://www.drupal.org/project/migrate/issues/2368563
   */
  protected $creation;

  /**
   * {@inheritdoc}
   */
  public function __construct($mapping = TRUE, $creation = FALSE) {
    parent::__construct('organism');

    $this->mapping = $mapping;
    $this->creation = $creation;
  }

  /**
   * Get key schema for the organism destination.
   *
   * @see MigrateDestination::getKeySchema
   *
   * @return array
   *   Returns the key schema.
   */
  public static function getKeySchema() {
    return array(
      'organism_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'organism ID',
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fields($migration = NULL) {
    return [
      'abbreviation' => t('Abbreviation (optional)'),
      'genus' => t('Locally used genus name'),
      'species' => t('Locally used species name'),
      'common_name' => t('Common name for this organism (optional)'),
      'comment' => t('Comment about this organism (optional)'),
    ];
  }

  /**
   * Delete a batch of organism data at once.
   *
   * @param array $ids
   *   Array of IDs to be deleted.
   */
  public function bulkRollback(array $ids) {
    // Nothing to do if we are only in mapping mode: we do not intend to alter
    // organisms referenced in the database in this case.
    if ($this->creation) {
      parent::bulkRollback($ids);
    }
  }

  /**
   * Import a single organism item.
   *
   * Actually we do not alter the organism table; rather, we keep track of
   * correspondences between source names and locally used ones, if any.
   *
   * @param object $object
   *   Experiment object to build. Prefilled with any fields mapped in the
   *   Migration.
   * @param object $row
   *   Raw source data object - passed through to prepare/complete handlers.
   *
   * @return array
   *   Array of key fields (organism_id only in this case) of the experiment
   *   item that was saved if successful. FALSE on failure.
   */
  public function import(stdClass $object, stdClass $row) {
    if ($this->mapping) {
      $organisms = chado_select_record('organism', ['organism_id'], [
        'genus' => $object->genus,
        'species' => $object->species,
      ]);
      if ($organisms && isset($organisms[0]->organism_id)) {
        return [$organisms[0]->organism_id];
      }
    }
    if ($this->creation) {
      parent::import($object, $row);
    }
    else {
      // No mapping found and we are not in creation mode: skip.
      return FALSE;
    }
  }

}
