<?php

/**
 * @file
 * Support for stock destinations.
 */

/**
 * Destination class implementing migration into stock table.
 */
class MigrateDestinationChadoStock extends MigrateDestinationChado {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct('stock');
  }

  /**
   * Get key schema for the stock destination.
   *
   * @see MigrateDestination::getKeySchema
   *
   * @return array
   *   Returns the key schema.
   */
  public static function getKeySchema() {
    return array(
      'stock_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'stock ID',
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fields($migration = NULL) {
    return [
      'name' => t('Human-readable local name (optional)'),
      'dbxref_id' => t('Primary stable identifier for this stock (optional)'),
      'uniquename' => t('Unique name'),
      'description' => t('Genetic description provided in the stock list (optional)'),
      'type_id' => t('Links to a controlled vocabulary of stock types'),
      'organism_id' => t('Links to an organism (optional)'),
      'is_obsolete' => t('Stock is obsolete (boolean, optional)'),
    ];
  }

}
