<?php

/**
 * @file
 * Support for experiment_stock destinations.
 */

/**
 * Destination class implementing migration into nd_experiment_stock table.
 */
class MigrateDestinationChadoExperimentStock extends MigrateDestinationChado {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct('nd_experiment_stock');
  }

  /**
   * Get key schema for the experiment destination.
   *
   * @see MigrateDestination::getKeySchema
   *
   * @return array
   *   Returns the key schema.
   */
  public static function getKeySchema() {
    return array(
      'nd_experiment_stock_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'experiment_stock ID',
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fields($migration = NULL) {
    return [
      'nd_experiment_id' => t('Links to an experiment'),
      'stock_id' => t('Links to a stock item'),
      'type_id' => t('Links to a controlled vocabulary of experiment_stock types'),
    ];
  }

}
