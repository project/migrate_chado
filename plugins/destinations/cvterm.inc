<?php

/**
 * @file
 * Support for cvterm destinations.
 */

/**
 * Destination class implementing migration into cvterm table.
 */
class MigrateDestinationChadoCvterm extends MigrateDestinationChado {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct('cvterm');
  }

  /**
   * Get key schema for the cvterm destination.
   *
   * @see MigrateDestination::getKeySchema
   *
   * @return array
   *   Returns the key schema.
   */
  public static function getKeySchema() {
    return array(
      'cvterm_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'cvterm ID',
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fields($migration = NULL) {
    return [
      'cv_id' => t('The controlled vocabulary or ontology or namespace this cvterm belongs to'),
      'name' => t('A concise human-readable name or label for the cvterm, uniquely identifies a cvterm within a cv'),
      'definition' => t('A human-readable text definition (optional)'),
      'dbxref_id' => t('Primary dbxref identifier, the unique global OBO identifier for this cvterm'),
      'is_obsolete' => t('Cvterm is obsolete (boolean, optional)'),
      'is_relationshiptype' => t('Used to indicate whether this  cvterm is an actual term or a relation'),
    ];
  }

}
