<?php

/**
 * @file
 * Support for experiment destinations.
 */

/**
 * Destination class implementing migration into nd_experiment table.
 */
class MigrateDestinationChadoExperiment extends MigrateDestinationChado {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct('nd_experiment');
  }

  /**
   * Get key schema for the experiment destination.
   *
   * @see MigrateDestination::getKeySchema
   *
   * @return array
   *   Returns the key schema.
   */
  public static function getKeySchema() {
    return array(
      'nd_experiment_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'experiment ID',
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fields($migration = NULL) {
    return [
      'nd_geolocation_id' => t('Links to a geolocation record'),
      'type_id' => t('Links to a controlled vocabulary of experiment types'),
    ];
  }

}
