<?php

/**
 * @file
 * Support for prop destinations.
 */

/**
 * Destination class implementing migration into "prop" table.
 */
class MigrateDestinationChadoProp extends MigrateDestinationChado {

  /**
   * Discover schema configuration.
   */
  public function __construct($base_table) {
    parent::__construct($base_table . 'prop', $base_table);
  }

  /**
   * Get key schema for the "prop" destination.
   *
   * @see MigrateDestination::getKeySchema
   *
   * @return array
   *   Returns the key schema.
   */
  public static function getKeySchema($base_table = NULL) {
    $key_schema = [];

    if ($base_table) {
      $table = $base_table . 'prop';
      $schema = chado_get_schema($table);
      $primary_key = $schema['primary key'][0];

      $key_schema = [
        $primary_key => [
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => t('@table ID', ['@table' => $table]),
        ],
      ];
    }

    return $key_schema;
  }

  /**
   * {@inheritdoc}
   */
  public function fields($migration = NULL) {
    return [
      $this->foreignKey => t('Related @base_table item', ['@base_table' => $this->baseTable]),
      'type_id' => t('Links to a controlled vocabulary'),
      'value' => t('Property value'),
      'rank' => t('Property rank (integer, optional)'),
    ];
  }

}
