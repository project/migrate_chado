<?php

/**
 * @file
 * Support for chado destinations.
 */

/**
 * Destination class implementing migration into chado schema.
 */
abstract class MigrateDestinationChado extends MigrateDestination {

  /**
   * Table to use for this migration.
   *
   * @var string
   */
  protected $table;

  /**
   * Primary key to use for this migration.
   *
   * @var string
   */
  protected $primaryKey;

  /**
   * Base table for ancillary tables (e.g. stock for stockprop).
   *
   * @var string
   */
  protected $baseTable;

  /**
   * Foreign key referencing a base table (e.g. stock_id).
   *
   * @var string
   */
  protected $foreignKey;

  /**
   * Discover schema configuration.
   */
  public function __construct($table, $base_table = '') {
    $this->table = $table;
    $schema = chado_get_schema($table);
    $this->primaryKey = $schema['primary key'][0];
    if ($base_table) {
      $this->baseTable = $base_table;
      $this->foreignKey = key($schema['foreign keys'][$base_table]['columns']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return t('chado (@table)', ['@table' => $this->table]);
  }

  /**
   * Delete a batch of items at once.
   *
   * @param array $ids
   *   Array of IDs to be deleted.
   */
  public function bulkRollback(array $ids) {
    migrate_instrument_start($this->table . '_delete_multiple');
    foreach ($ids as $id) {
      chado_delete_record($this->table, [$this->primaryKey => $id]);
    }
    migrate_instrument_stop($this->table . '_delete_multiple');
  }

  /**
   * Import a single item.
   *
   * @param object $object
   *   Object to build. Prefilled with any fields mapped in the Migration.
   * @param object $row
   *   Raw source data object - passed through to prepare/complete handlers.
   *
   * @return array
   *   Array of key fields of the item that was saved if successful. FALSE on
   *   failure.
   */
  public function import(stdClass $object, stdClass $row) {
    migrate_instrument_start($this->table . '_save');
    $values = array_intersect_key((array) $object, $this->fields());

    // We make sure we got a valid destination ID.
    if (!empty($row->migrate_map_destid1)) {
      $item = chado_update_record($this->table, [$this->primaryKey => $row->migrate_map_destid1], $values);
    }
    else {
      // Otherwise, we try insert first.
      // Grab Tripal output buffer to prevent unexpected outputs.
      ob_start();
      $item = chado_insert_record($this->table, $values);
      $tripal_message = ob_get_clean();
      // If it failed, maybe it has been already inserted before.
      if (!$item) {
        // If it has already been inserted, get current item.
        $item = chado_select_record($this->table, array('*'), $values);
        if (!$item) {
          // If not, throw an exception including Tripal message.
          throw new MigrateException(
            t(
              "Failed to insert (or retrieve) a Chado record in table !table. Record: !record\nTripal message: !tripal_message",
              array(
                '!table' => $this->table,
                '!record' => print_r($values, TRUE),
                '!tripal_message' => $tripal_message,
              )
            )
          );
        }
      }
    }
    migrate_instrument_stop($this->table . '_save');

    $this->numCreated++;

    return [$item[$this->primaryKey]];
  }

}
