<?php

/**
 * @file
 * Support for *_cvterm destinations (stock_cvterm, etc.).
 */

/**
 * Destination class implementing migration into *_cvterm table.
 */
class MigrateDestinationChadoFooCvterm extends MigrateDestinationChado {

  /**
   * {@inheritdoc}
   */
  public function __construct($base_table) {
    parent::__construct($base_table . '_cvterm', $base_table);
  }

  /**
   * Get key schema for the *_cvterm destination.
   *
   * @see MigrateDestination::getKeySchema
   *
   * @return array
   *   Returns the key schema.
   */
  public static function getKeySchema($base_table = NULL) {
    $key_schema = [];

    if ($base_table) {
      $table = $base_table . '_cvterm';
      $schema = chado_get_schema($table);
      $primary_key = $schema['primary key'][0];

      $key_schema = [
        $primary_key => [
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => t('@table ID', ['@table' => $table]),
        ],
      ];
    }

    return $key_schema;
  }

  /**
   * {@inheritdoc}
   */
  public function fields($migration = NULL) {
    return [
      $this->foreignKey => t('Related @base_table item', ['@base_table' => $this->baseTable]),
      'cvterm_id' => t('Related cvterm'),
      'pub_id' => t('Related publication'),
      'is_not' => t('Negate this term (boolean, optional)'),
      'rank' => t('cvterm rank (integer, optional)'),
    ];
  }

}
