<?php

/**
 * @file
 * Support for *_dbxref destinations (stock_dbxref, etc.).
 */

/**
 * Destination class implementing migration into *_dbxref tables.
 */
class MigrateDestinationChadoFooDbxref extends MigrateDestinationChado {

  /**
   * {@inheritdoc}
   */
  public function __construct($base_table) {
    parent::__construct($base_table . '_dbxref', $base_table);
  }

  /**
   * Get key schema for the *_dbxref destination.
   *
   * @see MigrateDestination::getKeySchema
   *
   * @return array
   *   Returns the key schema.
   */
  public static function getKeySchema($base_table = NULL) {
    $key_schema = [];

    if ($base_table) {
      $table = $base_table . '_dbxref';
      $schema = chado_get_schema($table);
      $primary_key = $schema['primary key'][0];

      $key_schema = [
        $primary_key => [
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => t('@table ID', ['@table' => $table]),
        ],
      ];
    }

    return $key_schema;
  }

  /**
   * {@inheritdoc}
   */
  public function fields($migration = NULL) {
    return [
      $this->foreignKey => t('Related @base_table item', ['@base_table' => $this->baseTable]),
      'dbxref_id' => t('Links to a dbxref record'),
      'is_current' => t('The is_current boolean indicates whether the linked dbxref is the current (official) dbxref for the linked stock (optional)'),
    ];
  }

}
