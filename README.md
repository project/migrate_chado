Migrate Chado is a collection of destination plugins to import biological data
to a [Chado][] database using [Migrate][]. It requires [Tripal][], a project
which provides integration with the Chado database schema and Drupal. It is
currently only available for Drupal 7, since Tripal has not been ported to
Drupal 8 yet.

[Chado]: http://gmod.org/wiki/Introduction_to_Chado
[Migrate]: https://www.drupal.org/project/migrate
[Tripal]: https://www.drupal.org/project/tripal

See the [project][] and [documentation][] pages for more information on how to
use this module.

[project]: https://www.drupal.org/project/migrate_chado
[documentation]: https://www.drupal.org/docs/7/modules/migrate-chado
